package com.cognizant.iot.devicecontroller.ChatUiLib.interfaces;


import com.cognizant.iot.devicecontroller.ChatUiLib.model.DataModel;

/**
 * Created by Bharath on 25/04/17.
 */

public interface ListInterface {

        void onProductClicked(DataModel productModel);

        void onOfferClicked(DataModel dataModel);

        void onSearchClicked(DataModel dataModel);

        void onChipClicked(String selectedChip);

        void onUserInput(String userInput);

        void onInvoiceNumber(String invoiceNumber);

}
