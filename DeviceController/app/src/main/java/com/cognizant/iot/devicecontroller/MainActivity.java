package com.cognizant.iot.devicecontroller;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.cognizant.iot.devicecontroller.ChatUiLib.ChatUI;
import com.cognizant.iot.devicecontroller.ChatUiLib.interfaces.ListInterface;
import com.cognizant.iot.devicecontroller.ChatUiLib.model.DataModel;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements ListInterface {

     /*The activity has to implement ListInterface to access the inputs from the user.*/

    ChatUI chatUI;
    RecyclerView chatRecyclerView;
    EditText userInput;
    ImageView sendImageView, voiceImageView;
    public static String CLIENT_ACCESS_TOKEN="acb17e4d0e3445d58850b96d47a28102";
    public static RequestQueue requestQueue;
    /*Api.ai cridentials : userid - RetailMateUser32, password - testtest100*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chatRecyclerView = (RecyclerView) findViewById(R.id.chat_recycler_view);

        voiceImageView = (ImageView) findViewById(R.id.voice);
        sendImageView = (ImageView) findViewById(R.id.send);
        userInput = (EditText) findViewById(R.id.user_input);

        chatUI = new ChatUI(this, chatRecyclerView, sendImageView, voiceImageView, userInput, getApplicationContext());

        requestQueue= Volley.newRequestQueue(getApplicationContext());
        /*
        * Creation of an object for ChatUI by passing
        * the MainActivity,
        * ReceyclerView,
        * ImageView of the send icon,
        * the EditText of the user input field and
        * the context
        */

        /**
         * The ChatUI is capable of displaying 7 types of items in its UI
         *
         *   1. Send Message, contains the text/input from the user.
         *                 ## layout_send.xml
         *
         *   2. Receive Message, contains the text from the bot.
         *                 ## layout_receive.xml
         *                 - call showReceiveMessage function of ChatUI with the response text as parameters.
         *
         *   3. Product List
         *                 ## card_product.xml
         *                 - Create an ArrayList of DataModel (3 params).
         *                 - DataModel object can be created by passing the product name, price info and imageURL/imageSourceFromDrawable/imageSourceFromMipmap.
         *                 - call showProductList function of ChatUI by passing the ArrayList as parameters.
         *                 ~ on clicking a product, onProductClicked override function will be invoked with the product object(DataModel) as params.
         *
         *   4. Offer List
         *                 ## card_offer_image.xml
         *                 - Create an ArrayList of DataModel (2 params).
         *                    # if three params are passed then the second string param will be ignored #
         *                 - DataModel object can be created by passing the OfferName/OfferDescription and imageURL/imageSourceFromDrawable/imageSourceFromMipmap.
         *                 - call showOfferList function of ChatUI by passing the ArrayList as parameters.
         *                 ~ on clicking an offer, onOfferClicked override function will be invoked with the offer object(DataModel) as params.
         *
         *   5. Search Result
         *                 ## card_info.xml
         *                 - Create an ArrayList of DataModel (3 params).
         *                 - DataModel object can be created by passing the ItemTitle, ItemDescription and imageURL/imageSourceFromDrawable/imageSourceFromMipmap.
         *                 - call showSearchResult function of ChatUI by passing the ArrayList as parameters.
         *
         *   6. Keys & Values with Title (eg. Exchange Rates)
         *                 ## list_recyc_vertical.xml
         *                 ## key_value_item.xml
         *                 - Create an HashMap with both the key and values as String data type.
         *                 - call showKeyValuePair function of ChatUI by passing the HashMap as parameters.
         *   7. Chips
         *                 ## chips_layout.xml
         *                 - Create an ArrayList of String.
         *                 - call showChipsList function of ChatUI by passing the ArrayList as parameters.
         *                 ~ on clicking the chip, it will destroy itself, set the selected chip's data as the user input message in the UI and the onUserInput override function will be invoked with the selected chip's data as params.
         *
         *
         *  ## The loading animation is handled in ChatUI, but if needed, the loading animation can be shown and removed  through showLoading() and hideLoading() of ChatUI.
         *
         *
         * */


        /*ArrayList<String> chipsList = new ArrayList();

        chipsList.add("hi!");
        chipsList.add("hello");
        chipsList.add("hey there!");
        chipsList.add("hi!");
        chipsList.add("hello");
        chipsList.add("hey there!");
        chipsList.add("hi!");
        chipsList.add("hello");
        chipsList.add("hey there!");
        chipsList.add("hi!");
        chipsList.add("hello");
        chipsList.add("hey there!");


        ArrayList<DataModel> productModels = new ArrayList<>();

        DataModel productModel = new DataModel("abc", "asd", "http://www.planwallpaper.com/static/images/327908-abstract-wallpapers.jpg");
        productModels.add(productModel);

        DataModel productModel1 = new DataModel("abc33", "asd33", R.mipmap.bot);
        productModels.add(productModel1);

        DataModel productModel2 = new DataModel("sdf", "sdf", R.mipmap.bot);
        productModels.add(productModel2);


        chatUI.showReceiveMessage("Hello");

        chatUI.showReceiveMessage("Helloqwe");

        chatUI.showProductList(productModels);

        chatUI.showSearchResult(productModels);

        chatUI.showOfferList(productModels);

        chatUI.showChipsList(chipsList);

        chatUI.getInvoiceNo();

        HashMap<String, String> stringMap = new HashMap<>();

        stringMap.put("1", "abc");
        stringMap.put("2", "abb");
        stringMap.put("2", "qww");
        stringMap.put("3", "qwe");

        chatUI.showKeyValuePair("Exchange Rate", stringMap);*/

    }

    @Override
    public void onProductClicked(DataModel productModel) {
        Toast.makeText(getApplicationContext(), productModel.getText() + " product", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onOfferClicked(DataModel dataModel) {
        Toast.makeText(getApplicationContext(), dataModel.getText() + " offer", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSearchClicked(DataModel dataModel) {
        Toast.makeText(getApplicationContext(), dataModel.getText() + " search", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onChipClicked(String selectedChip) {
        chatUI.setSendMessage(selectedChip);
    }

    @Override
    public void onUserInput(String userInput) {
        Toast.makeText(getApplicationContext(), userInput + " input", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onInvoiceNumber(String invoiceNumber) {
        Toast.makeText(getApplicationContext(), invoiceNumber + " invoiceNumber", Toast.LENGTH_LONG).show();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ChatUI.CHECK_VOICE_TO_TEXT && resultCode == RESULT_OK) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            chatUI.setSendMessage(result.get(0));
        }
    }
}
