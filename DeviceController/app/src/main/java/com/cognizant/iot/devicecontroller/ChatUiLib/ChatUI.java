package com.cognizant.iot.devicecontroller.ChatUiLib;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import com.cognizant.iot.devicecontroller.ChatAsyncTask.ApiAiTask;
import com.cognizant.iot.devicecontroller.ChatUiLib.adpater.ChatAdapter;
import com.cognizant.iot.devicecontroller.ChatUiLib.interfaces.UiInterface;
import com.cognizant.iot.devicecontroller.ChatUiLib.model.DataModel;
import com.cognizant.iot.devicecontroller.ChatUiLib.model.KeyValueModel;
import com.cognizant.iot.devicecontroller.ChatUiLib.model.TextModel;
import com.cognizant.iot.devicecontroller.MainActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import ai.api.AIDataService;
import ai.api.android.AIConfiguration;

/**
 * Created by Bharath on 24/04/17.
 */

public class ChatUI implements UiInterface {

    public static final int CHECK_VOICE_TO_TEXT = 111;
    MainActivity mainActivity;
    RecyclerView chatRecyclerView;
    ArrayList<Object> objectArrayList;
    ChatAdapter chatAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    ArrayList<Integer> elementType;
    ImageView send, voiceImageView;
    EditText userInput;
    TextToSpeech textToSpeech;
    Context context;
    AIConfiguration config;
    AIDataService aiDataService;


    boolean invoiceFlag = false;

    public ChatUI(final MainActivity mainActivity, final RecyclerView chatRecyclerView, ImageView send, ImageView voiceImageView, final EditText userInput, Context applicationContext) {
        this.mainActivity = mainActivity;
        this.chatRecyclerView = chatRecyclerView;
        this.objectArrayList = new ArrayList<>();
        this.elementType = new ArrayList<>();
        this.chatAdapter = new ChatAdapter(objectArrayList, elementType, applicationContext, this, mainActivity);
        this.mLayoutManager = new LinearLayoutManager(context);
        this.chatRecyclerView.setLayoutManager(mLayoutManager);
        this.chatRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        this.chatRecyclerView.setHasFixedSize(true);
//        this.chatRecyclerView.setNestedScrollingEnabled(true);
        this.chatRecyclerView.setAdapter(chatAdapter);
        this.send = send;
        this.voiceImageView = voiceImageView;
        this.userInput = userInput;

        config = new AIConfiguration(MainActivity.CLIENT_ACCESS_TOKEN,
                AIConfiguration.SupportedLanguages.English,
                AIConfiguration.RecognitionEngine.System);
        aiDataService = new AIDataService(config);

        chatRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override

            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                chatRecyclerView.scrollToPosition(objectArrayList.size() - 1);
            }
        });

        textToSpeech = new TextToSpeech(mainActivity.getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == textToSpeech.SUCCESS) {
                    textToSpeech.setLanguage(Locale.US);
                } else {
                    Toast.makeText(context, "Not supported in your device", Toast.LENGTH_SHORT).show();
                }
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("TAG", "send button Clicked");
                String inputText = userInput.getText().toString().trim();
                if (isOnline()) {
                    Log.e("TAG", "isOnline = true");

                    if (!inputText.equals(null) && !inputText.equals("")) {

                        if (elementType.size() < 0 && elementType.get(elementType.size() - 1) == Utils.CHIPS_LIST) {
                            chatAdapter.removeLastItem(objectArrayList.size() - 1);
                        }
                        if (invoiceFlag) {
                            mainActivity.onInvoiceNumber(inputText);
                            invoiceFlag = false;
                            userInput.setLongClickable(true);
                            userInput.setInputType(InputType.TYPE_CLASS_TEXT);
                            userInput.setHint("Ask me something..");
                        }
                        userInput.setText("");
                        setSendMessage(inputText);

                    }
                } else {
                    mainActivity.onUserInput("No Internet Connection !");
                }
            }
        });

        voiceImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                i.putExtra(RecognizerIntent.EXTRA_PROMPT, "You should be speaking!!");
                try {
                    mainActivity.startActivityForResult(i, CHECK_VOICE_TO_TEXT);
                } catch (ActivityNotFoundException a) {
                    a.printStackTrace();
                    Toast.makeText(context, "Problem with Voice!!!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public Context getAppContext() {
        return context;
    }

    protected boolean isOnline() {

        ConnectivityManager cm = (ConnectivityManager) mainActivity.getSystemService(mainActivity.getApplicationContext().CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info != null && info.isConnectedOrConnecting()) {
            return true;
        } else
            return false;
    }


    public void setSendMessage(String s) {
        if (!invoiceFlag) {

            hideLoading();
            TextModel textModel = new TextModel(s);
            elementType.add(Utils.SEND);
            objectArrayList.add(textModel);
            showLoading();
            updateChatUI();
        }

        //hide keyboard
        InputMethodManager imm = (InputMethodManager) mainActivity.getApplicationContext().getSystemService(mainActivity.getApplicationContext().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(send.getApplicationWindowToken(), 0);
        startAItask(s);

    }



    public void showReceiveMessage(String s) {
        hideLoading();
        TextModel textModel = new TextModel(s);
        elementType.add(Utils.RECEIVE);
        objectArrayList.add(textModel);
        updateChatUI();
        //FOR TEXT TO SPEECH
        textToSpeech(s);
    }


    public void showProductList(ArrayList<DataModel> productModels) {
        hideLoading();
        elementType.add(Utils.RECEIVE_PRODUCT);
        objectArrayList.add(productModels);
        updateChatUI();
    }

    public void showOfferList(ArrayList<DataModel> offerModels) {
        hideLoading();
        elementType.add(Utils.RECEIVE_OFFER_LIST);
        objectArrayList.add(offerModels);
        updateChatUI();
    }

    public void showSearchResult(ArrayList<DataModel> searchModels) {
        hideLoading();
        elementType.add(Utils.RECEIVE_SEARCH_LIST);
        objectArrayList.add(searchModels);
        updateChatUI();
    }

    public void showChipsList(ArrayList<String> chipsList) {
        hideLoading();
        elementType.add(Utils.CHIPS_LIST);
        objectArrayList.add(chipsList);
        updateChatUI();
    }

    public void getInvoiceNo() {
        hideLoading();
        invoiceFlag = true;
        userInput.setLongClickable(false); //to avoid pasting of text from clipboard
        userInput.setInputType(InputType.TYPE_CLASS_NUMBER);
        userInput.setHint("Please input Invoice Number");
    }

    public void showKeyValuePair(String title, HashMap<String, String> stringMap) {
        hideLoading();
        KeyValueModel keyValueModel = new KeyValueModel(title, stringMap);
        elementType.add(Utils.KEY_VALUE_PAIR);
        objectArrayList.add(keyValueModel);
        updateChatUI();
    }

    @Override
    public void updateChatUI() {
        chatAdapter.notifyDataSetChanged();

        chatRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                chatRecyclerView.smoothScrollToPosition(objectArrayList.size() - 1);
            }
        });
    }

    public void textToSpeech(String s) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            String utteranceId = mainActivity.getApplicationContext().hashCode() + "";
            textToSpeech.speak(s, TextToSpeech.QUEUE_FLUSH, null, utteranceId);

        } else {
            HashMap<String, String> map = new HashMap<>();
            map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
            textToSpeech.speak(s, TextToSpeech.QUEUE_FLUSH, map);
        }
    }

    public void showLoading() {
        elementType.add(Utils.SHOW_LOADING);
        objectArrayList.add(null);
    }

    public void hideLoading() {
        chatAdapter.hideLoading();
    }


    public void stopTextToSpech() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
    }

    private void startAItask(String requestString) {

        ApiAiTask aitask= new ApiAiTask(this,aiDataService);
        aitask.execute(requestString);
    }

}

