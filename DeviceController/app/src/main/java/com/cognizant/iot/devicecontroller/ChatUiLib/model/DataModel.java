package com.cognizant.iot.devicecontroller.ChatUiLib.model;

/**
 * Created by Bharath on 25/04/17.
 */

public class DataModel {

    String text;
    String info;
    int imageResourceId;
    String imageURL;

    public DataModel(String text, String info, int imageResourceId) {
        this.text = text;
        this.info = info;
        this.imageResourceId = imageResourceId;
        this.imageURL = null;
    }

    public DataModel(String text, String imageURL) {
        this.text = text;
        this.imageURL = imageURL;
    }

    public DataModel(String text, String info, String imageURL) {
        this.text = text;
        this.info = info;
        this.imageURL = imageURL;
    }

    public DataModel(String text, int imageResourceId) {
        this.text = text;
        this.imageResourceId = imageResourceId;
        this.imageURL = null;
    }

    public String getText() {
        return text;
    }

    public String getInfo() {
        return info;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public String getImageURL() {
        return imageURL;
    }

}
