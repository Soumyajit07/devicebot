package com.cognizant.iot.devicecontroller.ChatUiLib;

/**
 * Created by Bharath on 24/04/17.
 */

public class Utils {
    public static final int SEND = 0;
    public static final int RECEIVE = 1;
    public static final int RECEIVE_PRODUCT = 2;
    public static final int RECEIVE_OFFER_LIST = 3;
    public static final int RECEIVE_SEARCH_LIST = 4;
    public static final int CHIPS_LIST = 5;
    public static final int KEY_VALUE_PAIR = 6;
    public static final int SHOW_LOADING = 999;
}
