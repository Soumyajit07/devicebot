package com.cognizant.iot.devicecontroller.ChatAsyncTask;

import android.os.AsyncTask;
import android.util.Log;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.cognizant.iot.devicecontroller.ChatUiLib.ChatUI;
import com.cognizant.iot.devicecontroller.MainActivity;
import com.cognizant.iot.devicecontroller.UbidotsTask.ApiUbidots;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import ai.api.AIDataService;
import ai.api.AIServiceException;
import ai.api.model.AIError;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;

/**
 * Created by 540472 on 5/24/2017.
 * This Class sends user queries to API.AI and handles intent in switch case.
 */
public class ApiAiTask extends AsyncTask<String, Void, AIResponse> {
    private AIError aiError;
    ChatUI chatUI;
    AIDataService aiDataService;
    String acknowledgement_url="http://things.ubidots.com/api/v1.6/variables/5948d9807625423d97c6c459/values/?page_size=1&token=ov3lX63UgPf6KNL0OMQBmibtWlevLR";
    String intent;
    TimerTask task;

    public ApiAiTask(ChatUI chatUI, AIDataService aiDataService) {
        this.chatUI = chatUI;
        this.aiDataService = aiDataService;
    }

    @Override
    protected AIResponse doInBackground(final String... params) {
        final AIRequest request = new AIRequest();
        String query = params[0];
        request.setQuery(query);

        try {
            final AIResponse response = aiDataService.request(request);
            return response;
        } catch (final AIServiceException e) {
            aiError = new AIError(e);
            return null;
        }
    }

    @Override
    protected void onPostExecute(final AIResponse response) {
        if (response != null) {
            intent=response.getResult().getMetadata().getIntentName().toString().toLowerCase();
            boolean actionIncomplete = response.getResult().isActionIncomplete();
            String speech = response.getResult().getFulfillment().getSpeech();
            Log.e("TAG", "actionIncomplete = " + actionIncomplete
                    + "\nintent = " + intent
                    + "\nspeech = " + speech);
            switch (intent){

                case "greetings" :
                    chatUI.showReceiveMessage(speech);
                    break;
                case "bye" :
                    chatUI.showReceiveMessage(speech);
                    break;
                case "turn_on_light" :
                    Log.e("in","turn_on_light");
                    chatUI.showReceiveMessage(speech);
                    new ApiUbidots().execute(1);
                    look_for_acknowledgement();
                    break;
                case "turn_off_light" :
                    Log.e("in","turn_off_light");
                    chatUI.showReceiveMessage(speech);
                    new ApiUbidots().execute(0);
                    look_for_acknowledgement();
                    break;
                case "default fallback intent" :
                    break;
            }

        } else {
            Log.e("TAG","API.AI Error : "+aiError.getMessage().toString());
            chatUI.showReceiveMessage("I'm unable to reach my server please try later.");
        }
    }

    private void look_for_acknowledgement() {
        Timer timer = new Timer();
        task = new TimerTask () {
            @Override
            public void run () {
                volleycall();                       //send volley request here
            }
        };
        timer.schedule(task, 0, 7000); // 7000 is time in ms
    }

    public void volleycall()
    {
        final JsonObjectRequest acknowledgmentRequest=new JsonObjectRequest(Request.Method.GET, acknowledgement_url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try
                {
                    if(intent.contains("on")&&response.getJSONArray("results").getJSONObject(0).getString("value").equals("1.0"))
                    {
                        chatUI.showReceiveMessage("LED turned on succesfully!");
                        task.cancel();
                    }
                    else if(intent.contains("off")&&response.getJSONArray("results").getJSONObject(0).getString("value").equals("0.0"))
                    {
                        chatUI.showReceiveMessage("LED turned off succesfully!");
                        task.cancel();
                    }
                    Log.e("response",response.getJSONArray("results").getJSONObject(0).getString("value"));
                }catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //chatUI.showReceiveMessage("Acknowledgement not coming!! Connectivity Error..");
            }
        });
        MainActivity.requestQueue.add(acknowledgmentRequest);
    }
}
