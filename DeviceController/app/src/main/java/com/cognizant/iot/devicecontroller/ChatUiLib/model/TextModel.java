package com.cognizant.iot.devicecontroller.ChatUiLib.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Bharath on 24/04/17.
 */

public class TextModel {

    String text;
    String time;

    SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm aa");

    public TextModel(String text) {
        this.text = text;
        this.time = dateFormat.format(new Date()).toString();
    }

    public String getText() {
        return text;
    }

    public String getTime() {
        return time;
    }

}
