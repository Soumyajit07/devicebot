package com.cognizant.iot.devicecontroller.UbidotsTask;

import android.os.AsyncTask;

import com.ubidots.ApiClient;
import com.ubidots.Variable;

/**
 * Created by Guest_User on 16/06/17.
 */

public class ApiUbidots extends AsyncTask<Integer, Void, Void> {
    private final String API_KEY = "e7f67bba02796da3fd48198735d15a94947d56c8";
    private final String VARIABLE_ID = "593a4a977625426ab50a59ad";   //led variable id

    @Override
    protected Void doInBackground(Integer... params) {
        ApiClient apiClient = new ApiClient(API_KEY);
        Variable ledstatus = apiClient.getVariable(VARIABLE_ID);
        ledstatus.saveValue(params[0]);
        return null;
    }
}