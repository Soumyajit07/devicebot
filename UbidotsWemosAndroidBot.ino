#include "UbidotsMicroESP8266.h"

#define ID  "593a4a977625426ab50a59ad"  // Put here your Ubidots variable ID
#define ID_ACK "5948d9807625423d97c6c459"   //Acknowledgement variable ID
#define TOKEN  "ov3lX63UgPf6KNL0OMQBmibtWlevLR"  // Put here your Ubidots TOKEN
#define WIFISSID "Soumya" // Put here your Wi-Fi SSID
#define PASSWORD "soumya07" // Put here your Wi-Fi password

Ubidots client(TOKEN);
const int ledPin =  13;

void setup() {
    Serial.begin(115200);
    client.wifiConnection(WIFISSID, PASSWORD);
    pinMode(ledPin, OUTPUT);
    
    //client.setDebug(true); // Uncomment this line to set DEBUG on
}

void loop() {
    float value = client.getValue(ID);
    Serial.print("Value: ");
    Serial.println(value);
    delay(1000);
    if(value==0.00)
    {
      digitalWrite(ledPin, LOW);
      client.add(ID_ACK, 0);
    }
    else
    {
      digitalWrite(ledPin, HIGH);
      client.add(ID_ACK, 1);
    }
    client.sendAll(false);
}

//Pin mapping WEMOS ARDUINO
//static const uint8_t SDA = PIN_WIRE_SDA;
//static const uint8_t SCL = PIN_WIRE_SCL;
//
//static const uint8_t LED_BUILTIN = 2;
//static const uint8_t BUILTIN_LED = 2;
//
//static const uint8_t D0   = 16;
//static const uint8_t D1   = 5;
//static const uint8_t D2   = 4;
//static const uint8_t D3   = 0;
//static const uint8_t D4   = 2;
//static const uint8_t D5   = 14;
//static const uint8_t D6   = 12;
//static const uint8_t D7   = 13;(i connected on D7 at wemos,so i am putting ledpin as 13 here)
//static const uint8_t D8   = 15;
//static const uint8_t RX   = 3;
//static const uint8_t TX   = 1;

